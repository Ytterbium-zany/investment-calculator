$(function () {
    $(".decimal-input").inputmask(".99", {"value": ".00"});
    $(".decimal-input").blur(function () {
        var decimal = $(this).val();
        if(decimal != null && decimal != "") {
            $(this).val( decimal.replace(/_/g, "0") )
        } else {
            $(this).val( ".00" )
        }
    });

    $('[data-toggle="tooltip"]').tooltip();

    $('#calculation-form').validate({
        rules: {
            starting: {
                required: true,
                digits: true,
                max: 50000000
            },
            rate: {
                required: true,
                digits: true,
                max: 20
            },
            contribution: {
                required: true,
                digits: true,
                max: 50000000
            },
            period: {
                required: true,
                digits: true,
                max: 360
            },
        },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
                $(placement).append(error)
            } else {
                error.insertAfter(element);
            }
        },
        submitHandler: function (form) {

            var formArray = $(form).serializeArray();
            var formDict = {};
            $.each(formArray, function() {
                if (formDict[this.name] !== undefined) {
                    if (!formDict[this.name].push) {
                        formDict[this.name] = [formDict[this.name]];
                    }
                    formDict[this.name].push(this.value || '');
                } else {
                    formDict[this.name] = this.value || '';
                }
            });

            var starting = formDict['starting'].concat(formDict['starting-decimal']);
            var rate = formDict['rate'].concat(formDict['rate-decimal']);
            var contribution = formDict['contribution'].concat(formDict['contribution-decimal']);
            var period = formDict['period']

            $.ajax({url: "client.php",
                data: {
                    "starting": starting,
                    "rate": rate,
                    "contribution": contribution,
                    "period": period
                },
                dataType: "json",
                crossDomain: true,
                success: function (result) {
                    console.log(result);
                    if(result["statusCode"]=="0") {
                        var plans = result["investmentPlans"];

                        showResultTable(plans)

                    } else {
                        $("#result").html("<b>" + result["status"] + "</b> " + result["error"]);
                    }

            },
                error: function (error) {
                    console.log(error);
            }});

            return false;
        }
    });

});

function showResultTable(plans) {

    var table =     '<table class="table table-striped table-bordered">';
    table = table +     '<tbody>';
    table = table +         '<tr>';
    table = table +             '<th class="text-center">Month</th>';
    table = table +             '<th class="text-center">Contribution</th>';
    table = table +             '<th class="text-center">Total Contribution</th>';
    table = table +             '<th class="text-center">Interest</th>';
    table = table +             '<th class="text-center">Total Interest</th>';
    table = table +             '<th class="text-center">Balance</th>';
    table = table +         '</tr>'
    plans.forEach(function(plan) {
        table = table +     '<tr>';
        table = table +         '<td>' + plan["intervalNumber"] + '</td>';
        table = table +         '<td class="text-right">' + formatNumber(plan["contribution"]) + '</td>';
        table = table +         '<td class="text-right">' + formatNumber(plan["accumulatedContribution"]) + '</td>';
        table = table +         '<td class="text-right">' + formatNumber(plan["interest"]) + '</td>';
        table = table +         '<td class="text-right">' + formatNumber(plan["accumulatedInterest"]) + '</td>';
        table = table +         '<td class="text-right">' + formatNumber(plan["totalBalance"]) + '</td>';
        table = table +     '</tr>';
    });
    table = table +     '</tbody>';
    table = table + '</table>';

    var hasPlan = (plans.length > 0)? true: false;
    var lastPlan = plans[plans.length-1];
    table = table + '<div class="col-md-6 col-sm-12 col-xs-12 pull-right no-padding">';
    table = table + '<table class="table">';
    table = table +     '<tbody>';
    table = table +         '<tr>';
    table = table +             '<th>Final Balance</th>';
    if(hasPlan) table = table + '<th class="text-right">' + formatNumber(lastPlan["totalBalance"]) + '</th>';
    table = table +         '</tr>';
    table = table +         '<tr>';
    table = table +             '<th>Total Contribution</th>';
    if(hasPlan) table = table + '<th class="text-right">' + formatNumber(lastPlan["accumulatedContribution"]) + '</th>';
    table = table +         '</tr>';
    table = table +         '<tr>';
    table = table +             '<th>Total Interest</th>';
    if(hasPlan) table = table + '<th class="text-right">' + formatNumber(lastPlan["accumulatedInterest"]) + '</th>';
    table = table +         '</tr>';
    table = table +     '</tbody>';
    table = table + '</table>';
    table = table + '</div>';

    $("#result").html(table);
    $("#result-box").show("slide", {direction: "up" }, "slow");
}

function formatNumber(x) {

    x = Math.round(x * 100) / 100

    var parts = x.toString().split(".");
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    var decimal = parts[1];
    if (decimal == null || decimal == "") {
        parts[1] = "00";
    } else if (decimal.length == 1) {
        parts[1] = decimal*10;
    }

    return parts.join(".");
}